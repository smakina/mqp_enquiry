package com.salvation.mqpenquiry.retrofit.dto

data class CallLogDto(
    val name: String,
    val phone: String,
    val type: String,
    val date: Long,
    val duration: String,
)