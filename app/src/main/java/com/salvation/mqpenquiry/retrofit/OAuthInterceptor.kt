package com.salvation.mqpenquiry.retrofit

import com.salvation.mqpenquiry.App.Companion.token
import okhttp3.Interceptor

class OAuthInterceptor(private val tokenType: String ): Interceptor {
    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        var request = chain.request()
        request = request.newBuilder()
            .header("Authorization", "$tokenType ${token.value}").build()
        return chain.proceed(request)
    }
}
