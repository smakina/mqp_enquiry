package com.salvation.mqpenquiry.retrofit

import com.salvation.mqpenquiry.retrofit.dto.CallLogDto
import com.salvation.mqpenquiry.retrofit.dto.NUser
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface RetrofitInterface {
    @FormUrlEncoded
    @POST("customers")
    suspend fun saveCallLog(@Field("name") name: String, @Field("phone") phone: String, @Field("comment") comment: String): CallLogDto

    @FormUrlEncoded
    @POST("login")
    suspend fun authenticate(@Field("email") username: String, @Field("password") password: String): NUser
}