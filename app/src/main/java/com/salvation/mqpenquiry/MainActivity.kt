package com.salvation.mqpenquiry

import android.Manifest
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.work.*
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberMultiplePermissionsState
import com.salvation.mqpenquiry.ui.theme.MQPEnquiryTheme
import dagger.hilt.android.AndroidEntryPoint
import java.util.concurrent.TimeUnit

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    val permissionGiven = mutableStateOf(false)

    @OptIn(ExperimentalPermissionsApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        installSplashScreen().apply {
//            setKeepOnScreenCondition { permissionGiven.value }
            val intent = Intent(this@MainActivity, MainService::class.java)
            startService(intent)
        }

        setContent {
            MQPEnquiryTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    
                    val permissionGiven = remember {
                        mutableStateOf(false)
                    }

                    val multiPermissionState = rememberMultiplePermissionsState(
                        permissions = listOf(
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.READ_CALL_LOG,
                            Manifest.permission.RECEIVE_BOOT_COMPLETED
                        )
                    )

                    LaunchedEffect(key1 = true, block = {
                        startWorkManager()
                    })

                    LaunchedEffect(key1 = true, block = {
                        multiPermissionState.launchMultiplePermissionRequest()
                    })


                    LaunchedEffect(key1 = multiPermissionState.allPermissionsGranted, block = {
                        permissionGiven.value = true
                    })


                    permissionGiven.value = multiPermissionState.allPermissionsGranted
                    if ( permissionGiven.value) Login() //CallLogListPage()
                }
            }
        }
    }

    private fun startWorkManager() {
        val periodicSyncWorkManager = PeriodicWorkRequestBuilder<SyncWorkManager>(
            repeatInterval = 8,
            TimeUnit.MINUTES,
        ).setConstraints(
            Constraints.Builder()
                .build()
        ).build()

        val workManager = WorkManager.getInstance(this@MainActivity)
        val workInfo = workManager.getWorkInfosForUniqueWork("sync")

        workManager
            .enqueueUniquePeriodicWork(
                "sync",
                ExistingPeriodicWorkPolicy.KEEP,
                periodicSyncWorkManager
            )
    }
}

