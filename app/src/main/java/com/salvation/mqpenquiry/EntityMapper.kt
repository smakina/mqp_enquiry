package com.salvation.mqpenquiry

import com.salvation.mqpenquiry.db.dto.CallLogEntity

interface EntityMapper<Entity, T> {
    fun mapFromEntity(entity: Entity) : T
    fun mapToEntity(model : T) : Entity
}
