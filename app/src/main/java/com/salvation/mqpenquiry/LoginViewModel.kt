package com.salvation.mqpenquiry

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.salvation.mqpenquiry.repo.UserRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val savedStateHandle: SavedStateHandle,
    private val userRepo: UserRepo
): ViewModel() {
    private val _token: MutableState<Resource<String>> = mutableStateOf(Resource.idle<String>())
    val token = _token

    fun authenticate(username: String, password: String) {
        viewModelScope.launch {
            userRepo.authenticate(username = username, password = password).collectLatest {
                _token.value = it
            }
        }
    }

    fun resert() {
        _token.value = Resource.idle<String>()
    }


}