package com.salvation.mqpenquiry

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.TelephonyManager
import android.widget.Toast
import com.salvation.mqpenquiry.App.Companion.token
import com.salvation.mqpenquiry.repo.CallRepo
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

val CALL_FILTER = "android.intent.action.PHONE_STATE"

@AndroidEntryPoint
class CallReceiver : BroadcastReceiver() {

    @Inject
    lateinit var callRepo: CallRepo

    override fun onReceive(context: Context?, intent: Intent?) {
        if (CALL_FILTER == intent?.action) {
            context?.let { runfirstTime(intent = intent, context = it) }
        }
    }

    private fun runfirstTime(context: Context, intent: Intent) {
        log("SAVING................")
        val bundle = intent.extras
        val phone_number = bundle!!.getString("incoming_number")
        val stateStr = intent.extras!!.getString(TelephonyManager.EXTRA_STATE)
        var state = 0

        when (stateStr) {
            null -> return
            TelephonyManager.EXTRA_STATE_IDLE -> {
                CoroutineScope(Dispatchers.IO).launch {
                    log("TOKEN:::::::::: ${token.value}")
                    callRepo.saveCallLog(context = context)
                }
            }
            TelephonyManager.EXTRA_STATE_OFFHOOK -> {
                log(" OFF HOOK")
            }
            TelephonyManager.EXTRA_STATE_RINGING -> {
                log(" RINGING")
            }
        }

        if (phone_number == null || "" == phone_number) {
            return
        }
    }


}