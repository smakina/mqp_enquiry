package com.salvation.mqpenquiry

data class MCallLog(val phone: String, val type: String, val date: Long, val duration: String, val _synchronized: Boolean = false)
