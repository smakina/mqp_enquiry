package com.salvation.mqpenquiry

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun CallLogListPage() {

    val context = LocalContext.current

    val callLogViewModel: CallLogViewModel = hiltViewModel()
    val swipeRefreshState = rememberSwipeRefreshState(isRefreshing = true)
    val callLog = remember {
        mutableStateOf<List<MCallLog>>(emptyList())
    }

//    LaunchedEffect(key1 = true) {
//        callLogViewModel.saveCallLog(context = context)
//    }

    SwipeRefresh(state = swipeRefreshState, onRefresh = { }) {
        LazyColumn(modifier = Modifier.fillMaxWidth().padding(8.dp)) {
            callLogViewModel.groupList(callLog.value).value.forEach { map ->
                stickyHeader {
                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(bottom = 8.dp)
                    ) {
                        Text(
                            text = map.key,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(8.dp),
                            style = MaterialTheme.typography.h5,
                            color = MaterialTheme.colors.primary
                        )
                    }
                }

                items(map.value) { log ->
                    CallLogItem(log)
                    Spacer(modifier = Modifier.height(8.dp))
                }
            }

        }
    }

    val dataState = callLogViewModel.callLogList.value

    when (dataState.status) {
        Status.LOADING -> {
            swipeRefreshState.isRefreshing = true
        }
        Status.ERROR -> {
            swipeRefreshState.isRefreshing = false
        }

        Status.SUCCESS -> {
            swipeRefreshState.isRefreshing = false
            callLog.value = dataState.data!!
        }
        Status.IDLE -> {

        }
    }
}

@Composable
fun CallLogItem(log: MCallLog) {
    Card(modifier = Modifier.fillMaxWidth()) {
        Row(
            Modifier
                .fillMaxWidth()
                .padding(8.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Box(
                modifier = Modifier
                    .size(40.dp)
                    .clip(CircleShape)
                    .background(color = MaterialTheme.colors.primary.copy(0.12f)),
                contentAlignment = Alignment.Center
            ) {
                val icon = when (log.type) {
                    MISSED_CALL -> Icons.Default.CallMissed
                    OUTGOING_CALL -> Icons.Default.CallMade
                    INCOMING_CALL -> Icons.Default.CallReceived
                    else -> Icons.Default.CallReceived
                }
                Icon(
                    imageVector = icon,
                    contentDescription = "",
                    tint = MaterialTheme.colors.primary
                )
            }
            Column(Modifier.weight(1f)) {
                Text(text = log.phone)
                Text(text = log.type.lowercase())
            }
            Column(horizontalAlignment = Alignment.End) {
                Text(text = getTime(log.date))
                val icon = if(log._synchronized) Icons.Default.SyncAlt else Icons.Default.SyncProblem
                Icon(imageVector = icon, contentDescription = "")
            }

        }
    }
}