package com.salvation.mqpenquiry

import android.content.Context
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.salvation.mqpenquiry.repo.CallRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class CallLogViewModel @Inject constructor(
    private val savedStateHandle: SavedStateHandle,
    private val callRepo: CallRepo
) : ViewModel() {
    private val _callLogList: MutableState<Resource<List<MCallLog>>> =
        mutableStateOf(Resource.idle<List<MCallLog>>())
    val callLogList: State<Resource<List<MCallLog>>> = _callLogList

    init {
        getAllCallLogs()
    }

    private fun getAllCallLogs() {
        viewModelScope.launch(Dispatchers.IO) {
            callRepo.getCallLog().collectLatest {
                withContext(Dispatchers.Main) {
                    _callLogList.value = it
                }
            }
        }
    }

    fun saveCallLog(context: Context) {
        viewModelScope.launch(Dispatchers.IO) {
//            callRepo.saveCallLog(context = context)
        }
    }

    fun groupList(list: List<MCallLog>): State<Map<String, List<MCallLog>>> {
        val grouped: MutableState<Map<String, List<MCallLog>>> = mutableStateOf(emptyMap())
        viewModelScope.launch {
            grouped.value = list.groupBy { getShortDate(it.date) }
        }
        return grouped
    }
}