package com.salvation.mqpenquiry

import androidx.compose.foundation.clickable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Refresh
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Preview(showBackground = true)
@Composable
fun MainPage() {
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        floatingActionButton = {
            FloatingActionButton(onClick = { /*TODO*/ }) {
                Icon(imageVector = Icons.Outlined.Refresh, contentDescription = "")
            }
        }
    ) {
        Column(modifier = Modifier.fillMaxSize()) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 16.dp, bottom = 16.dp)
            ) {
                Text(text = "Merry Queen of Peace", style = MaterialTheme.typography.h4)
                Text(text = "JP2 enquiries", style = MaterialTheme.typography.subtitle1)
            }

            Row(
                Modifier
                    .fillMaxWidth()
                    .horizontalScroll(rememberScrollState())
            ) {
                Row(modifier = Modifier.offset(x = 16.dp)) {
                    SummaryItem()
                    SummaryItem()
                    SummaryItem()
                }
            }

            Spacer(modifier = Modifier.height(16.dp))
            Column(Modifier.fillMaxWidth()) {
                Row(
                    Modifier
                        .fillMaxWidth()
                        .padding(start = 16.dp, end = 16.dp)
                ) {
                    ActionItem(modifier = Modifier.weight(1f), title = "Create Contacts", icon = R.drawable.ic_add_user, onClick = {})
                    Spacer(modifier = Modifier.width(8.dp))
                    ActionItem(modifier = Modifier.weight(1f), title = "Export Contacts", icon = R.drawable.ic_export, onClick = {})
                }

                Spacer(modifier = Modifier.height(8.dp))

                Row(
                    Modifier
                        .fillMaxWidth()
                        .padding(start = 16.dp, end = 16.dp)
                ) {
                    ActionItem(modifier = Modifier.weight(1f), title = "Sync Contacts", icon = R.drawable.sync, onClick = {})
                    Spacer(modifier = Modifier.width(8.dp))
                    ActionItem(modifier = Modifier.weight(1f), title = "View Contacts", icon = R.drawable.ic_contact, onClick = {})
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun SummaryItem() {
    Card(modifier = Modifier
        .size(height = 120.dp, width = 140.dp)
        .clickable { }
        .padding(end = 8.dp), backgroundColor = MaterialTheme.colors.primary) {

    }
}

@Composable
fun ActionItem(modifier: Modifier = Modifier, title: String, icon: Int, onClick: ()-> Unit={}) {
    Card(modifier = modifier.height(120.dp)) {
        Column(
            modifier = Modifier.fillMaxWidth().padding(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Icon(
                modifier = Modifier.size(40.dp).padding(8.dp),
                painter = painterResource(id = icon),
                contentDescription = "",
                tint = MaterialTheme.colors.primary,
            )
            Text(text = title, style = MaterialTheme.typography.subtitle1)
        }
    }
}