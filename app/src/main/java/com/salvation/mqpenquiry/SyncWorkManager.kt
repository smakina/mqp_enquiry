package com.salvation.mqpenquiry

import android.content.Context
import androidx.core.app.NotificationCompat
import androidx.hilt.work.HiltWorker
import androidx.work.CoroutineWorker
import androidx.work.ForegroundInfo
import androidx.work.WorkerParameters
import com.salvation.mqpenquiry.repo.CallRepo
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext
import javax.inject.Inject
import kotlin.random.Random

@HiltWorker
class SyncWorkManager @AssistedInject constructor(
    @Assisted val context: Context,
    @Assisted val workerParameters: WorkerParameters,
    val callRepo: CallRepo
) : CoroutineWorker(context, params = workerParameters) {

    override suspend fun doWork(): Result {
        withContext(Dispatchers.IO) {
            try {
                callRepo.saveCallLog(context = context)
                Result.success()
            } catch (e: Exception) {
                log("${e.message}")
                return@withContext Result.failure()
            }
        }
        showNotification()
        return Result.success()
    }

    private suspend fun showNotification() {
        setForeground(
            ForegroundInfo(
                Random.nextInt(),
                NotificationCompat.Builder(context, "sync_data")
                    .setSmallIcon(R.drawable.sync)
                    .setContentText("Syncing")
                    .setContentTitle("Syncing contacts")
                    .build()
            )
        )
    }
}