package com.salvation.mqpenquiry

import android.app.Activity
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.OutlinedButton
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.salvation.mqpenquiry.App.Companion.token

@Preview(showBackground = true)
@Composable
fun Login() {
    val loginViewModel: LoginViewModel = hiltViewModel()

    var username by remember { mutableStateOf(TextFieldValue()) }
    var password by remember { mutableStateOf(TextFieldValue()) }
    val authenticating = remember { mutableStateOf(false) }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.White)
            .padding(horizontal = 16.dp, vertical = 16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = painterResource(id = R.drawable.mqplogo),
            contentDescription = "",
            modifier = Modifier.height(200.dp)
        )
        Spacer(modifier = Modifier.height(8.dp))

        if (authenticating.value) {
            CircularProgressIndicator()
        } else {
            Column(modifier = Modifier.fillMaxWidth()) {
                OutlinedTextField(
                    value = username,
                    label = { Text(text = "Username") },
                    onValueChange = { username = it },
                    modifier = Modifier.fillMaxWidth(),
                    maxLines = 1,
                    singleLine = true
                )
                Spacer(modifier = Modifier.height(8.dp))
                OutlinedTextField(
                    value = password,
                    label = { Text(text = "Password") },
                    onValueChange = { password = it },
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
                    modifier = Modifier.fillMaxWidth(),
                    maxLines = 1,
                    singleLine = true
                )
                Spacer(modifier = Modifier.height(8.dp))
                OutlinedButton(
                    onClick = {
                    if (username.text.isNotEmpty() && password.text.isNotEmpty()) {
                        loginViewModel.authenticate(
                            username = username.text,
                            password = password.text
                        )
                    }
                }) {
                    Text(text = "Login")
                }
            }
        }
    }

    val tokenState = loginViewModel.token.value

    when (tokenState.status) {
        Status.LOADING -> authenticating.value = true
        Status.SUCCESS -> {
            authenticating.value = false
            token.value = tokenState.data!!
            loginViewModel.resert()
            Toast.makeText(
                LocalContext.current,
                " Thank you ${username.text}. Have a nice day",
                Toast.LENGTH_LONG
            ).show()
            val activity = LocalContext.current as Activity
            activity.finish()
        }
        Status.ERROR -> {
            Toast.makeText(
                LocalContext.current,
                " Login failed: ${tokenState.message}",
                Toast.LENGTH_LONG
            ).show()
            loginViewModel.resert()
        }
        Status.IDLE -> {}
    }
}