package com.salvation.mqpenquiry

import android.content.Context
import android.database.Cursor
import android.provider.CallLog
import android.util.Log
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

val TAG = "MQP"
const val MISSED_CALL = "MISSED"
const val OUTGOING_CALL = "OUTGOING"
const val INCOMING_CALL = "INCOMING"

fun log(any: Any) {
    Log.d(TAG, "$any")
}

fun getCallDetails(context: Context): List<MCallLog> {
    val mCallLogList: MutableList<MCallLog> = ArrayList()
    val managedCursor: Cursor = context.contentResolver.query(
        CallLog.Calls.CONTENT_URI, null,
        null, null, null
    ) ?: return emptyList()

    val name = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME)
    val number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER)
    val type = managedCursor.getColumnIndex(CallLog.Calls.TYPE)
    val date = managedCursor.getColumnIndex(CallLog.Calls.DATE)
    val duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION)
    val sb = StringBuilder()
    while (managedCursor.moveToNext()) {
        val name = managedCursor.getString(name)
        val phNumber = managedCursor.getString(number)
        val callType = managedCursor.getString(type)
        val callDate = managedCursor.getLong(date)
        val callDuration = managedCursor.getString(duration)
        var dir = ""
        val dircode = callType.toInt()
        when (dircode) {
            CallLog.Calls.OUTGOING_TYPE -> dir = OUTGOING_CALL
            CallLog.Calls.INCOMING_TYPE -> dir = INCOMING_CALL
            CallLog.Calls.MISSED_TYPE -> dir = MISSED_CALL
        }
        sb.append(phNumber).append("\n")
        mCallLogList.add(
            MCallLog(
                formatPhoneNumber(phNumber),
                dir,
                callDate,
                "" + callDuration,
                false
            )
        )
    }

//        HelperMethods.writeToFile(sb.toString(), this);
    managedCursor.close()
    val mCallLogs: Array<MCallLog> = mCallLogList.toTypedArray()
    return mCallLogList
}

fun formatPhoneNumber(number: String): String {
    var number = number
    if (number.length < 8) return ""
    val v1 = number.substring(0, 3)
    val v2 = number.substring(0, 4)
    if (v2 == "+265" || v1 == "265") {
        number = number.replace("+265", "0")
            .replace("265", "0")
            .replace(" ", "")
            .trim { it <= ' ' }
    }
    return number.replace(" ", "")
}

fun getShortDate(date: Long): String {
    return try {
//        val parser = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        formatter.format(date).toString()
    } catch (e: Exception) {
        log("${e.message}")
        "00/00/00"
    }
}

fun getDateFormat(date: Long): String {
    return try {
//        val parser = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm a", Locale.getDefault())
        formatter.format(date).toString()
    } catch (e: Exception) {
        log("${e.message}")
        "00/00/00"
    }
}

fun getTime(date: Long): String {
    return try {
//        val parser = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val formatter = SimpleDateFormat("HH:mm a", Locale.getDefault())
        formatter.format(date).toString()
    } catch (e: Exception) {
        log("${e.message}")
        "00/00/00"
    }
}

