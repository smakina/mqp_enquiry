package com.salvation.mqpenquiry

import android.app.Service
import android.content.Intent
import android.content.IntentFilter
import android.os.IBinder

class MainService : Service() {
    lateinit var callReceiver: CallReceiver
    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        callReceiver = CallReceiver()
        val intentFilter = IntentFilter()
        intentFilter.addAction(CALL_FILTER)

        registerReceiver(callReceiver, intentFilter)
    }
}