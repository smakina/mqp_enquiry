package com.salvation.mqpenquiry.ui.theme

import androidx.compose.ui.graphics.Color

val MainColor = Color(0xFF173862)
val MainVariant = Color(0xFF072348)
val MainLightColor = Color(0xFF2A4D79)
val MainLighterColor = Color(0xFF476790)
val SecondaryColor = Color(0xFFFEE655)
val LightGrey = Color(0xFFF2F2F2)

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)