package com.salvation.mqpenquiry.di

import com.salvation.mqpenquiry.db.UserDao
import com.salvation.mqpenquiry.repo.UserRepo
import com.salvation.mqpenquiry.retrofit.RetrofitInterface
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Provides
    fun provideUserRepo(retrofitInterface: RetrofitInterface, userDao: UserDao) =
        UserRepo(retrofitInterface = retrofitInterface, userDao = userDao)
}