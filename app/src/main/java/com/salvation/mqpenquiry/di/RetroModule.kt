package com.salvation.mqpenquiry.di

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.salvation.mqpenquiry.API_ADDRESS
import com.salvation.mqpenquiry.PhoneStateListener
import com.salvation.mqpenquiry.db.CallLogDao
import com.salvation.mqpenquiry.db.CallLogMapper
import com.salvation.mqpenquiry.db.LocalDatabase
import com.salvation.mqpenquiry.db.UserDao
import com.salvation.mqpenquiry.repo.CallRepo
import com.salvation.mqpenquiry.retrofit.OAuthInterceptor
import com.salvation.mqpenquiry.retrofit.RetrofitInterface
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class RetroModule {

    @Singleton
    @Provides
    fun provideGsonBuilder(): Gson {
        return GsonBuilder()
            .create()
    }

    @Provides
    fun provideRetrofit(gson: Gson): Retrofit.Builder {
        val client = OkHttpClient.Builder()
            .addInterceptor(OAuthInterceptor("Bearer"))
            .build()
        return Retrofit.Builder()
            .client(client)
            .baseUrl(API_ADDRESS)
            .addConverterFactory(GsonConverterFactory.create(gson))
    }

    @Provides
    fun provideRetrofitService(retrofit: Retrofit.Builder): RetrofitInterface {
        return retrofit.build()
            .create(RetrofitInterface::class.java)
    }

    @Provides
    fun provideCallLOgMapper() = CallLogMapper()

    @Provides
    fun provideCallRepo(
        retrofitInterface: RetrofitInterface,
        callLogDao: CallLogDao,
        callLogMapper: CallLogMapper,
        userDao: UserDao
    ) =
        CallRepo(
            retrofitInterface = retrofitInterface,
            callLogDao = callLogDao,
            callLogMapper = callLogMapper,
            userDao = userDao
        )

    @Provides
    fun provideDataBase(@ApplicationContext context: Context): LocalDatabase {
        return LocalDatabase.getInstance(context = context)
    }

    @Provides
    fun provideDao(localDatabase: LocalDatabase) = localDatabase.callLogDao()

    @Provides
    fun provideUserDao(localDatabase: LocalDatabase) = localDatabase.userDao()

    @Provides
    fun providePhoneStateListener(@ApplicationContext context: Context, callRepo: CallRepo) =
        PhoneStateListener(context = context, callRepo = callRepo)
}