package com.salvation.mqpenquiry.db

import androidx.room.*
import com.salvation.mqpenquiry.db.dto.CallLogEntity

@Dao
interface CallLogDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(vararg callLogEntity: CallLogEntity)

    @Update
    suspend fun update(vararg callLogEntity: CallLogEntity)

    @Query("SELECT * FROM tbl_call_log")
    fun getCallLog(): List<CallLogEntity>

    @Query("SELECT * FROM tbl_call_log WHERE _synchronized = 0")
    fun getUnsynchronisedMessages(): List<CallLogEntity>
}