package com.salvation.mqpenquiry.db.dto

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "tbl_call_log", indices = [Index(value = ["date", "phone"], unique = true)])
data class CallLogEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val phone: String,
    val type: String,
    val date: Long,
    val duration: String,
    var _synchronized: Boolean
)