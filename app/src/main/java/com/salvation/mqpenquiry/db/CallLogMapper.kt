package com.salvation.mqpenquiry.db

import com.salvation.mqpenquiry.EntityMapper
import com.salvation.mqpenquiry.MCallLog
import com.salvation.mqpenquiry.db.dto.CallLogEntity
import com.salvation.mqpenquiry.retrofit.dto.CallLogDto

class CallLogMapper : EntityMapper<CallLogEntity, MCallLog> {
    override fun mapFromEntity(entity: CallLogEntity): MCallLog {
        return MCallLog(
            phone = entity.phone,
            type = entity.type,
            date = entity.date,
            duration = entity.duration,
            _synchronized = entity._synchronized
        )
    }

    override fun mapToEntity(model: MCallLog): CallLogEntity {
        return CallLogEntity(
            id = 0,
            phone = model.phone,
            type = model.type,
            date = model.date,
            duration = model.duration,
            _synchronized = false
        )
    }

    fun mapToDto(entity: CallLogEntity): CallLogDto {
        return CallLogDto(
            name = entity.phone,
            phone = entity.phone,
            type = entity.type,
            date = entity.date,
            duration = entity.duration
        )
    }

    fun mapAllToEntity(callDetails: List<MCallLog>): List<CallLogEntity> {
        return callDetails.map {
            mapToEntity(it)
        }
    }

    fun mapAllFromEntity(callDetails: List<CallLogEntity>): List<MCallLog> {
        return callDetails.map {
            mapFromEntity(it)
        }
    }
}