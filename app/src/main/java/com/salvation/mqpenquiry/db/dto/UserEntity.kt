package com.salvation.mqpenquiry.db.dto

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tbl_user")
class UserEntity(@PrimaryKey val token: String)