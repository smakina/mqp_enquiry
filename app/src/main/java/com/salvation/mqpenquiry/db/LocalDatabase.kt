package com.salvation.mqpenquiry.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.salvation.mqpenquiry.db.dto.CallLogEntity
import com.salvation.mqpenquiry.db.dto.UserEntity

@Database(entities = [CallLogEntity::class, UserEntity::class], version = 1)
abstract class LocalDatabase : RoomDatabase() {
    abstract fun callLogDao(): CallLogDao
    abstract fun userDao(): UserDao

    companion object {
        @Volatile
        private var INSTANCE : LocalDatabase? = null

        fun getInstance(context : Context) : LocalDatabase {
            synchronized(this) {
                return INSTANCE?: Room.databaseBuilder(
                    context.applicationContext,
                    LocalDatabase::class.java,
                    "mqp_enquiry"
                ).build().also {
                    INSTANCE = it
                }
            }
        }
    }
}