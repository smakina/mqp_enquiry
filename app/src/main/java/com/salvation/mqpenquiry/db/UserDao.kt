package com.salvation.mqpenquiry.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.salvation.mqpenquiry.db.dto.UserEntity

@Dao
interface UserDao {
    @Insert(onConflict = REPLACE)
    suspend fun insert(userEntity: UserEntity)

    @Query("SELECT * from tbl_user")
    fun getUser(): UserEntity?
}