package com.salvation.mqpenquiry.repo

import android.content.Context
import com.salvation.mqpenquiry.*
import com.salvation.mqpenquiry.App.Companion.token
import com.salvation.mqpenquiry.db.CallLogDao
import com.salvation.mqpenquiry.db.CallLogMapper
import com.salvation.mqpenquiry.db.UserDao
import com.salvation.mqpenquiry.db.dto.CallLogEntity
import com.salvation.mqpenquiry.retrofit.RetrofitInterface
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flow

class CallRepo(
    private val retrofitInterface: RetrofitInterface,
    private val callLogDao: CallLogDao,
    private val callLogMapper: CallLogMapper,
    private val userDao: UserDao
) {

    suspend fun saveCallLog(context: Context) {
        val callDetails = getCallDetails(context = context)
        callLogDao.insert(*callLogMapper.mapAllToEntity(callDetails).toTypedArray())

        val user = userDao.getUser()
        if (user != null){
            token.value = user.token
            syncMessages()
        }
    }

    suspend fun syncMessages() {
        log("TOKEN ---++++ ${token.value}")
        val result: Flow<List<CallLogEntity>> = flow {
            val entities = callLogDao.getUnsynchronisedMessages().map {
                if (it.phone != "") {
                    try {
                        val comment = "${it.type} on ${getDateFormat(it.date)}"
                        val callLog =
                            retrofitInterface.saveCallLog(it.phone, it.phone, comment = comment)
                        it._synchronized = true
                        callLogDao.update(it)
                        log("Saved!")
                    } catch (e: Exception) {
                        log("sync message failed ${it.phone}")
                        log(" ${e.message} ")
                        log("Delaying ... coroutine")
                        kotlinx.coroutines.delay(60000)
                        log("continue coroutine")
                    }
                }
                it
            }
            emit(entities)
        }

        result.collectLatest {
            log("Completed call log ${it.size}")
        }

    }

    suspend fun getCallLog(): Flow<Resource<List<MCallLog>>> = flow {
        emit(Resource.loading(null))
        try {
            val logs = callLogMapper.mapAllFromEntity(callLogDao.getCallLog())
            emit(Resource.success(logs))
        } catch (e: Exception) {
            emit(Resource.error(e.message, null))
        }
    }

}