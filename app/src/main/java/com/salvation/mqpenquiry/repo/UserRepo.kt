package com.salvation.mqpenquiry.repo

import com.salvation.mqpenquiry.Resource
import com.salvation.mqpenquiry.User
import com.salvation.mqpenquiry.db.UserDao
import com.salvation.mqpenquiry.db.dto.UserEntity
import com.salvation.mqpenquiry.log
import com.salvation.mqpenquiry.retrofit.RetrofitInterface
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class UserRepo(private val retrofitInterface: RetrofitInterface, private val userDao: UserDao) {
    suspend fun authenticate(username: String, password: String): Flow<Resource<String>> = flow{
        try {
           val res = retrofitInterface.authenticate(username, password)
            userDao.insert(UserEntity(res.token))
            emit(Resource.success(res.token))
        } catch (e: Exception) {
            emit(Resource.error(e.message, null))
        }
    }
}